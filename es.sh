docker run -d \
        --name es \
        --restart=always \
        -p 9200:9200 \
        -p 9300:9300 \
        -e "xpack.security.enabled=false" \
        -e "xpack.security.http.ssl.enabled=false" \
        -e "discovery.type=single-node" \
        elasticsearch:8.1.0
