# /usr/bin/env python

import json

from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200/")
IDX = "test"


result = es.search(index=IDX, query={"match_all": {}})

print(json.dumps(result, indent=2))
