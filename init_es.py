# /usr/bin/env python

from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200/")


IDX = "test"
MAPPING = {
    "dynamic_templates": [
        {
            "nft_date": {
                "match": "meta_date_*",
                "mapping": {
                    "type": "date",
                },
            }
        },
        {
            "nft_int": {
                "match": "meta_int_*",
                "mapping": {
                    "type": "integer",
                },
            }
        },
        {
            "nft_long": {
                "match": "meta_long_*",
                "mapping": {
                    "type": "long",
                },
            }
        },
        {
            "nft_text": {
                "match": "meta_text_*",
                "mapping": {
                    "type": "keyword",
                },
            }
        },
        {
            "nft_float": {
                "match": "meta_float_*",
                "mapping": {
                    "type": "float",
                },
            }
        },
        {
            "nft_double": {
                "match": "meta_double_*",
                "mapping": {
                    "type": "double",
                },
            }
        },
    ],
    "_source": {"excludes": ["meta_*"]},
    "properties": {
        "nft_id": {
            "type": "integer",
        },
        "network_id": {"type": "integer"},
        "contract_id": {
            "type": "integer",
        },
        "token_id": {
            "type": "keyword",
        },
        "name": {
            "type": "text",
        },
        "rank": {
            "type": "integer",
        },
        "has_open_order": {"type": "boolean"},
        "order_id": {
            "type": "integer",
        },
        "type": {"type": "keyword"},
        "created_at": {"type": "date", "format": "epoch_second"},
        "end_at": {"type": "date", "format": "epoch_second"},
        "is_bundle": {"type": "boolean"},
        "price": {"type": "float"},
        "currency": {"type": "keyword"},
        "orders": {"type": "object", "enabled": "false"},
    },
}

es.indices.create(index=IDX, settings={"number_of_shards": 1, "number_of_replicas": 1})
es.indices.put_mapping(index=IDX, body=MAPPING)

print(es)
