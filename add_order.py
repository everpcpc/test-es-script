# /usr/bin/env python

import logging

from elasticsearch import Elasticsearch


logging.basicConfig(level=logging.INFO)

es = Elasticsearch("http://localhost:9200/")
IDX = "test"

order = {
    "order_id": 12345,
    "order_type": "sell",
    "price": 1.6,
    "currency": "0x0000000000000000000000000000000000000000",
    "created_at": 1648016123,
    "end_at": 1648102456,
    "market": "opensea",
}

script = """
def newOrderMarket = params['order']['market'];
def newOrderID = String.valueOf(params['order']['order_id']);
if (!ctx._source.orders.containsKey(newOrderMarket)) {
    ctx._source.orders[newOrderMarket] = new HashMap();
}
ctx._source.orders[newOrderMarket][newOrderID] = params['order'];

def current;
def currentPrice = ctx._source.price;
for (marketOrders in ctx._source.orders.entrySet()) {
    for (order in marketOrders.getValue().entrySet()) {
        def price = order.getValue().price;
        if (price <= currentPrice) {
            current = order.getValue();
            currentPrice = price;
        }
    }
}

if (current != null) {
    ctx._source.has_open_order = true;
    ctx._source.type = current.order_type;
    ctx._source.order_id = current.order_id;
    ctx._source.price = current.price;
    ctx._source.currency = current.currency;
    ctx._source.created_at = current.created_at;
    ctx._source.end_at = current.end_at;
}

"""

es.update(
    index=IDX,
    id=683701,
    script={"lang": "painless", "source": script, "params": {"order": order}},
)
