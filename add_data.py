# /usr/bin/env python

from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200/")
IDX = "test"


data = {
    "nft_id": 683701,
    "network_id": 1,
    "contract_id": 561,
    "token_id": "8684",
    "rank": None,
    "has_open_order": True,
    "order_id": 1126839,
    "type": "sell",
    "created_at": 1648016540,
    "end_at": 1648102938,
    "price": 1.9,
    "currency": "0x0000000000000000000000000000000000000000",
    "is_bundle": False,
    "orders": {
        "internal": {
            "1126839": {
                "order_id": 1126839,
                "order_type": "sell",
                "market": "internal",
                "price": 1.9,
                "currency": "0x0000000000000000000000000000000000000000",
                "created_at": 1648016540,
                "end_at": 1648102938,
            }
        }
    },
}

es.index(index=IDX, id=data["nft_id"], document=data)
