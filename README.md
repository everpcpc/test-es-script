# test

### steps

```bash
python init_es.py
python add_data.py
python search_data.py
python add_order.py
python search_data.py
```

### output
```
<Elasticsearch([{'host': 'localhost', 'port': 9200}])>
```

```json
{
  "took": 425,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 1,
      "relation": "eq"
    },
    "max_score": 1.0,
    "hits": [
      {
        "_index": "test",
        "_id": "683701",
        "_score": 1.0,
        "_source": {
          "end_at": 1648102938,
          "contract_id": 561,
          "created_at": 1648016540,
          "type": "sell",
          "is_bundle": false,
          "network_id": 1,
          "token_id": "8684",
          "nft_id": 683701,
          "price": 1.9,
          "rank": null,
          "currency": "0x0000000000000000000000000000000000000000",
          "orders": {
            "internal": {
              "1126839": {
                "order_id": 1126839,
                "order_type": "sell",
                "market": "internal",
                "price": 1.9,
                "currency": "0x0000000000000000000000000000000000000000",
                "created_at": 1648016540,
                "end_at": 1648102938
              }
            }
          },
          "has_open_order": true,
          "order_id": 1126839
        }
      }
    ]
  }
}
```

```
INFO:elasticsearch:GET http://localhost:9200/ [status:200 request:0.014s]
INFO:elasticsearch:POST http://localhost:9200/test/_update/683701 [status:200 request:0.156s]
```

```json
{
  "took": 850,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 1,
      "relation": "eq"
    },
    "max_score": 1.0,
    "hits": [
      {
        "_index": "test",
        "_id": "683701",
        "_score": 1.0,
        "_source": {
          "end_at": 1648102456,
          "contract_id": 561,
          "created_at": 1648016123,
          "type": "sell",
          "is_bundle": false,
          "network_id": 1,
          "token_id": "8684",
          "nft_id": 683701,
          "price": 1.6,
          "rank": null,
          "currency": "0x0000000000000000000000000000000000000000",
          "orders": {
            "internal": {
              "1126839": {
                "order_id": 1126839,
                "order_type": "sell",
                "market": "internal",
                "price": 1.9,
                "currency": "0x0000000000000000000000000000000000000000",
                "created_at": 1648016540,
                "end_at": 1648102938
              }
            },
            "opensea": {
              "12345": {
                "market": "opensea",
                "end_at": 1648102456,
                "price": 1.6,
                "created_at": 1648016123,
                "currency": "0x0000000000000000000000000000000000000000",
                "order_id": 12345,
                "order_type": "sell"
              }
            }
          },
          "has_open_order": true,
          "order_id": 12345
        }
      }
    ]
  }
}
```
